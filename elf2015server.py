# Echo server program
# # -*- coding: utf-8 -*-
import binascii
import struct 
import socket
import time
import errno
from time import sleep
import sys

HOST = ''                 # Symbolic name meaning all available interfaces
PORT = 50007              # Arbitrary non-privileged port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)

#Esto se debe reemplazar por una rutina que capture datos desde el microcontrolador.

s_control = 0xdeadbeef
s_trans_r = 0x10101010
s_trans_i = 0xbeefdead
s_trans_o = 0x99989796

busy = False;


while 1:



	if busy == False:
		try:
			print 'Awaiting for connection'
			conn, addr = s.accept()
			print 'Connected by', addr
			busy = True
		except KeyboardInterrupt:
			print
			sys.exit()


	s_control = s_control+1
	s_trans_r = s_trans_r+2
	s_trans_o = s_trans_o+3
	s_trans_i = s_trans_i+4


	try: 
		sleep(1);
		#data = conn.recv(1024)
		#if not data: break
		pk = struct.pack('I I I I',s_control,s_trans_r,s_trans_o,s_trans_i)
		conn.sendall(pk)
		print 'iSent: ',binascii.hexlify(pk)

	except IOError, e:
		if e.errno == errno.EPIPE:
			conn.close()

			#Esto debería cerra un thread y ya.
			print 'Peer closed socket.'
			busy = False




	except KeyboardInterrupt:
		conn.close()
		print
 		print 'Thy will is this server to be closed.'
		sys.exit()







